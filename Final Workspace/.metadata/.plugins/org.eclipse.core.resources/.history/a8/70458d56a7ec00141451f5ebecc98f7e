package engine;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import entity.Entity;
import entity.Position;
import asciiPanel.AsciiPanel;

public class Engine extends Thread {

	public static void main(String []args) {
		Engine asciiGenetics = new Engine();
		asciiGenetics.start();
	}

	private JFrame frame = new JFrame("Ascii Genetics");
	private AsciiPanel panel;
	private Environment e  = new Environment();
	private Entity[][] enviroChar = new Entity[39][86];
	private final Color BACKGROUND_COLOR = Color.GRAY;
	private final Color FOREGROUND_COLOR = Color.white;
	private final int fontWidth = 9;
	private final int fontHeight = 16;
	private int highlightX = 0;
	private int highlightY = 0;

	private static boolean isRunning = true;
	private static KeyListener listener;

	public Engine() {
		initListener();
		panel = new AsciiPanel(110, 41);
		panel.write("+--------------------------------------------------------------------------------------+|+------Credits------+",0,0, Color.black, BACKGROUND_COLOR);
		byte loop = 1;
		for(int i=1;i<=39;i++) {
			panel.write("|",0,i, Color.black, BACKGROUND_COLOR);
			panel.write("|", 109, i, Color.black, BACKGROUND_COLOR);
			if(i%10 == 0) {
				loop++;
				if(loop == 2)
					panel.write("||+----A----+----B----+",87, i, Color.black, BACKGROUND_COLOR);
				else if(loop == 3)
					panel.write("||+----C----+----D----+",87, i, Color.black, BACKGROUND_COLOR);
				else if(loop == 4)
					panel.write("||+---------+---------+",87, i, Color.black, BACKGROUND_COLOR);
			} else {
				if(i>10&&i<30) {
					panel.write("|",99,i, Color.black, BACKGROUND_COLOR);
				} else 
					panel.write("|                     ",87,i, Color.black, BACKGROUND_COLOR);
			}
			panel.write("|||",87,i, Color.black, BACKGROUND_COLOR);
		}
		panel.write("+--------------------------------------------------------------------------------------+|+-------------------+",0,40, Color.black, BACKGROUND_COLOR);

		panel.write("Kevin   Park", 92, 4, Color.white, BACKGROUND_COLOR);
		panel.write("Eric    Lindau", 92, 5, Color.white, BACKGROUND_COLOR);
		panel.write("Fisher  Darling", 92, 6, Color.white, BACKGROUND_COLOR);

		panel.write("  |\\_/|  ", 95, 33, Color.white, BACKGROUND_COLOR);
		panel.write(" / @ @ \\ ", 95, 34, Color.white, BACKGROUND_COLOR);
		panel.write("( > � < )", 95, 35, Color.white, BACKGROUND_COLOR);
		panel.write(" `>>x<<\' ", 95, 36, Color.white, BACKGROUND_COLOR);
		panel.write(" /  O  \\ ", 95, 37, Color.white, BACKGROUND_COLOR);

		frame.setBackground(BACKGROUND_COLOR);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(panel);
		frame.pack();
		frame.addKeyListener(listener);
		//frame.setSize(screenSize.width, screenSize.height);
		frame.setSize((fontWidth*112)-3, (fontHeight*43)+2);
		frame.setFocusable(true);
		frame.setVisible(true);

		for(int i=0;i<e.getSize();i++) {
			Entity t = e.getEntity(i);
			Position tp = t.getPosition();
			write(tp.getRow(), tp.getCol(), t);
		}
	}

	public void newGeneration() {
		enviroChar = new Entity[39][86];
		e.processGen();
		for(int i=0;i<e.getSize();i++) {
			Entity t = e.getEntity(i);
			Position tp = t.getPosition();
			write(tp.getRow(), tp.getCol(), t);
		}
	}
	
	public void run() {
		int sleepCounter = 0;
		while(isRunning) {
			for(int i=0;i<enviroChar.length;i++) {
				for(int j=0;j<enviroChar[i].length;j++) {
					if((i==highlightY||i==highlightY+1)&&(j==highlightX||j==highlightX+1)&&enviroChar[i][j]!=null)
						panel.write(enviroChar[i][j].getGender(), j+1, i+1, enviroChar[i][j].getColor(), Color.yellow);
					else if(enviroChar[i][j]!=null)
						panel.write(enviroChar[i][j].getGender(), j+1, i+1, enviroChar[i][j].getColor(), BACKGROUND_COLOR);
					else if((i==highlightY||i==highlightY+1)&&(j==highlightX||j==highlightX+1))
						panel.write('-', j+1, i+1, FOREGROUND_COLOR, Color.yellow);
					else
						panel.write('-', j+1, i+1, FOREGROUND_COLOR, BACKGROUND_COLOR);	
				}
			}
			int width=90, height=11;
			for(int a=0;a<4;a++) {
				int xinc = 0;
				int yinc = 0;
				switch(a) {
				case 1:
					xinc++;
					break;
				case 2:
					yinc++;
					break;
				case 3:
					xinc++;
					yinc++;
					break;
				}
				if(enviroChar[highlightY+yinc][highlightX+xinc]==null) {
					for(int b=0;b<9;b++) {
						for(int c=0;c<9;c++) {
							panel.write('-', width+c, height+b, FOREGROUND_COLOR, BACKGROUND_COLOR);
						}
					}
				} else {
					if(sleepCounter==25) {
						char[][] figure = enviroChar[highlightY+yinc][highlightX+xinc].getFigure();
						Color[][] figureColor = enviroChar[highlightY+yinc][highlightX+xinc].getFigureColor();
						for(int b=0;b<9;b++) {
							for(int c=0;c<9;c++) {
								if(figureColor[b][c]==null)
									panel.write(figure[b][c], width+c, height+b, FOREGROUND_COLOR, BACKGROUND_COLOR);
								else
									panel.write(figure[b][c], width+c, height+b, figureColor[b][c], BACKGROUND_COLOR);
							}
						}
					}
				}
				if(width==90) {
					width = 100;
				} else {
					width = 90;
					height+=10;
				}
			}
			if(sleepCounter==25)
				sleepCounter=0;
			else
				sleepCounter++;
			SwingUtilities.updateComponentTreeUI(frame);
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				JOptionPane.showMessageDialog(null, "Character Switcher Crashed.", "InterruptedException", JOptionPane.ERROR_MESSAGE);
				System.err.println();
			} 
		}
		System.exit(0);
	}

	private void write(int y, int x, Entity character) {
		enviroChar[y][x] = character;
	}

	public void initListener() {
		listener = new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				parseInput(e);
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// Not used
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// Not used
			}
		};
	}

	/**
	 * Parses through all currently pressed keys and determines the desired
	 * effects
	 * 
	 * @return
	 */
	public void parseInput(KeyEvent e) {
		switch (e.getKeyCode()) {
		case 37:
			if(highlightX!=0)
				highlightX--;
			break;
		case 38:
			if(highlightY!=0)
				highlightY--;
			break;
		case 39:
			if(highlightX!=84)
				highlightX++;
			break;
		case 40:
			if(highlightY!=37)
				highlightY++;
			break;
		case 32:
			newGeneration();
			break;
		}
	}

}
